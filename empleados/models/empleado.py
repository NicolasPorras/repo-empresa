from django.db import models

class Empleado(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100, null=False)
    cedula = models.CharField(max_length=10, null=False)
    cargo = models.CharField(max_length=45, null=False)
    email = models.EmailField(max_length = 100, null=False)
    celular = models.CharField(max_length=10, null=False)