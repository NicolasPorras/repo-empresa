from django.db.models import fields
from rest_framework import serializers
from ..models.empleado import Empleado

class CrearEmpleadoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Empleado
        fields = ["nombre","cedula","cargo","email","celular"]

class MostrarEmpleadoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Empleado
        fields = ["id","nombre","cedula","cargo","email","celular"]