from rest_framework import views
from rest_framework.response import Response
from ..models.empleado import Empleado
from ..serializers.empleado_serializer import MostrarEmpleadoSerializer
from ..serializers.empleado_serializer import CrearEmpleadoSerializer


class EmpleadoView(views.APIView):
    def get(self, request):
        lista_empleados = Empleado.objects.all()
        serializer = MostrarEmpleadoSerializer(lista_empleados, many=True)
        return Response(serializer.data, 200)

    def post(self, request):
        datos_json = request.data
        serializer = CrearEmpleadoSerializer(data=datos_json)
        if serializer.is_valid():
            serializer.save()
            return Response({"mensaje": "empleado creado"}, 200)
        else:
            return Response(serializer.errors, 400)
        
