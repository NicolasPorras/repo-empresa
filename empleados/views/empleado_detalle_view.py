from rest_framework import views
from rest_framework.response import Response
from ..models.empleado import Empleado
from ..serializers.empleado_serializer import MostrarEmpleadoSerializer
from ..serializers.empleado_serializer import CrearEmpleadoSerializer


class EmpleadoDetalleView(views.APIView):
    def get(self, request, pk):
        try:
            empleado = Empleado.objects.get(pk=pk)
            serializer = MostrarEmpleadoSerializer(empleado)
            return Response(serializer.data, 200)
        except:
            return Response({"mensaje": "empleado no existe"}, 400)

    def put(self, request, pk):
        try:
            empleado = Empleado.objects.get(pk=pk)
            serializer = MostrarEmpleadoSerializer(empleado, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, 200)
            else:
                return Response(serializer.errors, 400)
        except:
            return Response({"mensaje": "empleado no existe"}, 400)
        
    def delete(self, request, pk):
        try:
            empleado = Empleado.objects.get(pk=pk)
            empleado.delete()
            return Response({"mensaje": "empleado eliminado"}, 200)
        except:
            return Response({"mensaje": "empleado no existe"}, 400)